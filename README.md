# **Scrapy parser for Swiss house listings**

## **Immoscout24**

### **Dependencies**
*  [Scrapy 1.5.2](http://doc.scrapy.org/en/latest/intro/install.html)
*  [pymongo 3.7.2](https://api.mongodb.com/python/current/)

The first crawler is for the https://www.immoscout24.ch website.

Some notes for this website:

*  It has a hidden API
*  Sample request of the API 

   [api url](https://react-api.immoscout24.ch/v2/en/properties?l=4147&r=40&s=3&t=2)

   which corresponds to this search request 
   
   [proper url](https://www.immoscout24.ch/en/house/buy/city-zuerich?r=40)

*  Parameter l on the request is the location id (4147 is Zurich)
*  r = radius
*  s = type of the house (2 is flat, 3 is house etc)
*  t = rent or buy (1 for rent 2 for buy)
*  **Run the script by typing:** `scrapy crawl immoscout24`
*  **Data is stored in a mongodb (localhost), so you have to install mongodb**


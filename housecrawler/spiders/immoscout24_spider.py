import json
import scrapy
from housecrawler.items import HouseItem, AgentItem
import unicodedata
import string

all_letters = string.ascii_letters + " .,;'-_"
n_letters = len(all_letters)


"""Turn a Unicode string to plain ASCII, thanks to https://stackoverflow.com/a/518232/2809427"""


def unicode_to_ascii(s):
    return ''.join(
        c for c in unicodedata.normalize('NFD', s)
        if unicodedata.category(c) != 'Mn'
        and c in all_letters
    )


class ListingSpider(scrapy.Spider):
    name = "immoscout24"
    allowed_domains = ["https://react-api.immoscout24.ch"]

    """Explanation for the url. l = location  id, s = type of house. 2 is flat, t = rent or buy (1 for rent),
    r = radius (max is 100), pn = pagenumber"""

    """4147 is for zurich"""
    l = "4147"
    s = "2"
    t = "1"
    r = "100"
    pn = "1"
    url = "https://react-api.immoscout24.ch/v2/en/properties?l={0}&s={1}&t={2}&r={3}&pn={4}"
    start_urls = [url.format(l, s, t, r, pn)]

    """I am doing a little trick on the parse function. Since I don't know the number of pages I am getting from the 
    API the current page and the last page and I use recursion, calling the parse function from inside. Check lines 
    67-71"""

    def parse(self, response):
        json_response = json.loads(response.body_as_unicode())

        for item in json_response["properties"]:
            house_item = HouseItem()
            house_item["source_name"] = "immoscout24"
            house_item["source_id"] = item["id"]
            house_item["url"] = "https://www.immoscout24.ch" + item["propertyDetailUrl"]
            house_item["rooms"] = item["numberOfRooms"] if "numberOfRooms" in item.keys() else None
            house_item["price"] = item["price"] if "price" in item.keys() else None
            house_item["street"] = unicode_to_ascii(item["street"])
            house_item["zip"] = item["zip"] if "zip" in item.keys() else None
            house_item["city"] = item["cityName"] if "cityName" in item.keys() else None
            house_item["latitude"] = item["latitude"] if "latitude" in item.keys() else None
            house_item["longitude"] = item["longitude"] if "longitude" in item.keys() else None
            house_item["rent_or_buy"] = "rent" if self.t == "1" else "buy"
            house_item["living_surface"] = item["surfaceLiving"] if "surfaceLiving" in item.keys() else None
            house_item["total_surface"] = item["surfaceProperty"] if "surfaceProperty" in item.keys() else None
            house_item["images"] = [image["url"].replace("{width}", str(image["originalWidth"])).replace("{height}",
                                    str(image["originalHeight"])).replace("{resizemode}", "3")
                                    .replace("{quality}", "90") for image in item["images"]] \
                if "images" in item.keys() else None
            yield house_item

        current_page = json_response["pagingInfo"]["currentPage"]
        last_page = json_response["pagingInfo"]["totalPages"]
        if current_page < last_page:
            next_page = current_page + 1
            new_request = scrapy.Request(url=self.url.format(self.l, self.s, self.t, self.r, str(next_page)),
                                         callback=self.parse, dont_filter=True)
            yield new_request

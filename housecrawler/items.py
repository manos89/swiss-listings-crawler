import scrapy
from scrapy.loader.processors import TakeFirst


class HouseItem(scrapy.Item):
    source_name = scrapy.Field(output_processor=TakeFirst())
    source_id = scrapy.Field(output_processor=TakeFirst())
    url = scrapy.Field(output_processor=TakeFirst())
    rooms = scrapy.Field(output_processor=TakeFirst())
    price = scrapy.Field(output_processor=TakeFirst())
    street = scrapy.Field(output_processor=TakeFirst())
    zip = scrapy.Field(output_processor=TakeFirst())
    city = scrapy.Field(output_processor=TakeFirst())
    latitude = scrapy.Field(output_processor=TakeFirst())
    longitude = scrapy.Field(output_processor=TakeFirst())
    rent_or_buy = scrapy.Field(output_processor=TakeFirst())
    living_surface = scrapy.Field(output_processor=TakeFirst())
    total_surface = scrapy.Field(output_processor=TakeFirst())
    images = scrapy.Field()

class AgentItem(scrapy.Item):
    agent_name = scrapy.Field(output_processor=TakeFirst())
    company_name = scrapy.Field(output_processor=TakeFirst())
    phone = scrapy.Field(output_processor=TakeFirst())
    company_street = scrapy.Field(output_processor=TakeFirst())
    company_zip = scrapy.Field(output_processor=TakeFirst())
    url = scrapy.Field(output_processor=TakeFirst())


import logging
import pymongo


class MongoPipeline(object):
    collection_name = "listings"

    def __init__(self, mongo_uri, mongo_db):
        self.mongo_uri = mongo_uri
        self.mongo_db = mongo_db

    @classmethod
    def from_crawler(cls, crawler):
        ## pull in information from settings.py
        return cls(
            mongo_uri=crawler.settings.get('MONGO_URI'),
            mongo_db=crawler.settings.get('MONGO_DATABASE'),

        )

    def listing_exists(self, item):
        try:
            results = self.db[self.collection_name].find({"source_name": dict(item)["source_name"],
                                                          "source_id": dict(item)["source_id"]})
            resultscount = results.limit(1).count()
        except:
            resultscount = 0
        if resultscount > 0:
            return True
        else:
            return False

    def open_spider(self, spider):
        ## initializing spider
        ## opening db connection
        self.client = pymongo.MongoClient(self.mongo_uri)
        self.db = self.client[self.mongo_db]

    def close_spider(self, spider):
        ## clean up when spider is closed
        self.client.close()

    def process_item(self, item, spider):
        game_exists = self.listing_exists(item)
        if game_exists is False:
            self.db[self.collection_name].insert(dict(item))

        return item
